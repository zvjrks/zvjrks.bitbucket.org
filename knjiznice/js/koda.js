
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var ehrId = $("#preberiEHRid").val();
  
  ehrId[0]="79c49de6-e6f5-489a-9012-9c003ce6ad7a";
  ehrId[1]="b51965b9-f2c0-42f1-81fb-e9fe8e1a4ee5";
  ehrId[2]="0d93d4b7-8c2e-4029-bd41-29f1c8769467";
  
  $('#preberiEHRid').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#dodajVitalnoEHR").val(podatki[2]);
    $("#dodajStarost").val(podatki[3]);
    $("#dodajSpol").val(podatki[4]);
    $("#dodajVitalnoTelesnaTeza").val(podatki[5]);
    document.getElementById("narediSporocilo").innerHTML=podatki[2];
  });
  
  
  return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function kreirajEHRzaBolnika() {
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();

	if (!ime || !priimek || ime.trim().length == 0 ||
      priimek.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              $("#preberiEHRid").val(ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
	}
}

function dodajMeritveVitalnihZnakov() {
	var ehrId = $("#dodajVitalnoEHR").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var starost = $("#dodajStarost").val();
	var spol = $("#dodajSpol").val();
  
  
	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza

		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        $("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-success fade-in'>" +
          res.meta.href + ".</span>");
          
      },
      error: function(err) {
      	$("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}
	
	var kcal = izracunKcal();
	document.getElementById("generirajKcal").innerHTML=kcal+" kcal";
	generirajObrok();
  
	console.log(kcal);
	console.log(generirajObrok());
}

function preberiEHRodBolnika() {
	var ehrId = $("#dodajVitalnoEHR").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			/*----------------------*/
  		},
  		error: function(err) {
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
  		}
		});
	}
}

function izracunKcal(){
  var spol = $("#dodajSpol").val();
  var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
  var starost = $("#dodajStarost").val();
  var kcal;
  
  if(spol == "m" || spol == "M"){
    if(starost <=3){
      kcal = (60.9*telesnaTeza-54)*1.3;
    }
    if(starost >=4 && starost <=10){
      kcal = (22.7*telesnaTeza+495)*1.3;
    }
    if(starost >=11 && starost <=18){
      kcal = (17.5*telesnaTeza+651)*1.3;
    }
    if(starost >=19 && starost <= 29){
    kcal=(15.3*telesnaTeza+678)*1.3;
    }
    if(starost >=30 && starost <=59){
      kcal=(11.6*telesnaTeza+879)*1.3;
    }
    if(starost >=60 && starost <=74){
      kcal=(11.9*telesnaTeza+700)*1.3;
    }
    if(starost>=75){
      kcal=(8.4*telesnaTeza+820)*1.3;
    }
  }
  else{
    if(starost <=3){
      kcal = (61*telesnaTeza-51)*1.3;
    }
    if(starost >=4 && starost <=10){
      kcal = (22.5*telesnaTeza+499)*1.3;
    }
    if(starost >=11 && starost <=18){
      kcal = (12.2*telesnaTeza+746)*1.3;
    }
    if(starost >=19 && starost <= 29){
    kcal=(14.7*telesnaTeza+496)*1.3;
    }
    if(starost >=30 && starost <=59){
      kcal=(8.6*telesnaTeza+829)*1.3;
    }
    if(starost >=60 && starost <=74){
      kcal=(9.2*telesnaTeza+688)*1.3;
    }
    if(starost>=75){
      kcal=(9.8*telesnaTeza+624)*1.3;
    }
  }
  
  return Math.round(kcal);
}

function generirajObrok(){
  var zajtrk = [];
  var kosilo = [];
  var vecerja = [];
  
  var obr1;
  var obr2;
  var obr3;
  
  var kcal = izracunKcal();
  
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    var myObj = JSON.parse(this.responseText);
      for(var i = 0; i<myObj.length; i++){
        zajtrk.push(myObj[i].zajtrk);
        kosilo.push(myObj[i].kosilo);
        vecerja.push(myObj[i].vecerja);
      }
      for(var i=0; i<zajtrk.length; i++){
        for(var j=0; j<2; j++){
          if(kcal <= 1200){
            obr1 = zajtrk[0][Math.floor(Math.random() * 2)];
            obr2 = kosilo[0][Math.floor(Math.random() * 2)];
            obr3 = vecerja[0][Math.floor(Math.random() * 2)];
          }
          if(kcal >=1201 && kcal <=1400){
            obr1 = zajtrk[1][Math.floor(Math.random() * 2)];
            obr2 = kosilo[1][Math.floor(Math.random() * 2)];
            obr3 = vecerja[1][Math.floor(Math.random() * 2)];
          }
          if(kcal >=1401 && kcal <=1600){
            obr1 = zajtrk[2][Math.floor(Math.random() * 2)];
            obr2 = kosilo[2][Math.floor(Math.random() * 2)];
            obr3 = vecerja[2][Math.floor(Math.random() * 2)];
          }
          if(kcal >=1601 && kcal <=1800){
            obr1 = zajtrk[3][Math.floor(Math.random() * 2)];
            obr2 = kosilo[3][Math.floor(Math.random() * 2)];
            obr3 = vecerja[3][Math.floor(Math.random() * 2)];
          }
          if(kcal >=1801 && kcal <=2000){
            obr1 = zajtrk[4][Math.floor(Math.random() * 2)];
            obr2 = kosilo[4][Math.floor(Math.random() * 2)];
            obr3 = vecerja[4][Math.floor(Math.random() * 2)];
          }
          if(kcal >=2001 && kcal <=2200){
            obr1 = zajtrk[5][Math.floor(Math.random() * 2)];
            obr2 = kosilo[5][Math.floor(Math.random() * 2)];
            obr3 = vecerja[5][Math.floor(Math.random() * 2)];
          }
          if(kcal >=2201 && kcal <=2400){
            obr1 = zajtrk[6][Math.floor(Math.random() * 2)];
            obr2 = kosilo[6][Math.floor(Math.random() * 2)];
            obr3 = vecerja[6][Math.floor(Math.random() * 2)];
          }
          if(kcal >=2401 && kcal <=2600){
            obr1 = zajtrk[7][Math.floor(Math.random() * 2)];
            obr2 = kosilo[7][Math.floor(Math.random() * 2)];
            obr3 = vecerja[7][Math.floor(Math.random() * 2)];
          }
          if(kcal >=2601 && kcal <=2800){
            obr1 = zajtrk[8][Math.floor(Math.random() * 2)];
            obr2 = kosilo[8][Math.floor(Math.random() * 2)];
            obr3 = vecerja[8][Math.floor(Math.random() * 2)];
          }
          if(kcal >=2801 && kcal <=3000){
            obr1 = zajtrk[9][Math.floor(Math.random() * 2)];
            obr2 = kosilo[9][Math.floor(Math.random() * 2)];
            obr3 = vecerja[9][Math.floor(Math.random() * 2)];
          }
          if(kcal >3000){
            obr1 = zajtrk[10][Math.floor(Math.random() * 2)];
            obr2 = kosilo[10][Math.floor(Math.random() * 2)];
            obr3 = vecerja[10][Math.floor(Math.random() * 2)];
          }
        }
      }
      console.log(obr1+" "+obr2+" "+obr3);
      document.getElementById("generirajZajtrk").innerHTML=obr1;
      document.getElementById("generirajKosilo").innerHTML=obr2;
      document.getElementById("generirajVecerjo").innerHTML=obr3;
    }

  };
  xmlhttp.open("GET", "https://bitbucket.org/zvjrks/zvjrks.bitbucket.org/raw/c922d925f7da3b1d9f8196aa9626e6776e4cdb21/knjiznice/json/obroki.json", true);
  xmlhttp.send();
}

function zemljevid(){
  var lat =[];
  var lng=[];
  var kooridinate=[];
  
   var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var myObj = JSON.parse(this.responseText);
          for(var i = 0; i<myObj.length; i++){
            

          }
      }
      
    };

  xmlhttp.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xmlhttp.send();
}
zemljevid();

function refresh(){
  document.getElementById("kreirajIme").value="";
  document.getElementById("kreirajPriimek").value="";
  document.getElementById("dodajVitalnoEHR").value="";
  document.getElementById("dodajStarost").value="";
  document.getElementById("dodajSpol").value="";
  document.getElementById("dodajVitalnoTelesnaTeza").value="";
  
}